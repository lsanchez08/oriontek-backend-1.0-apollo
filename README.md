# README #
This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
OrionTek Test

### How do I get set up? ###
- Check if Node is installed running: node -v otherwise install it https://nodejs.org/en/
- Check if you have mongo installed on your machine and run it using: "mongo"
- Create a folder to download the project using terminal: "mkDir oriontek"
- Change directory to the recently created folder using terminal: "cd oriontek"
- Clone the project using terminal: "git clone <project-directory>"
- Run "npm i" in Visual Studio Code's terminal
- To start the project run "npm run start"

```
PROJECT SHOULD BE RUNNING IN http://localhost:9001/api
```


### Users endpoints ###
    this.Router.get("/user/:id", userController.get);
    this.Router.get("/user", userController.getAll);
    this.Router.post("/user", userController.registerUser);
    this.Router.put("/user/:id", userController.update);
    this.Router.delete("/user/:id", userController.delete);


#### Example post for a user  ###
Can copy paste the following example as raw in postman:
{
    "name": "David",
    "lastName": "Ortiz",
    "email": "davidortiz@mlb.com",
    "gender": 1,
    "phoneNumber": "8090001911",
    "status": 1,
    "address": {"city": "Santo Domingo", 
    "country": "Dominican Republic",
    "street": "Av. 27 de Febrero"}
}


### Who do I talk to? ###
Repo owner or admin 